Source: mypaint
Section: graphics
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Andrew Chadwick <a.t.chadwick@gmail.com>,
 Vincent Cheng <vcheng@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 gettext,
 libgtk-3-dev,
 libjson-c-dev,
 liblcms2-dev,
 libpng-dev,
 libmypaint-dev (>= 1.5~),
 librsvg2-dev <!nocheck>,
 python3-dev,
 python-gi-dev,
 python3-gi,
 python3-gi-cairo,
 python3-matplotlib,
 python3-numpy,
 python3-setuptools,
 mypaint-brushes (>= 1.5~) <!nocheck>,
 swig,
Standards-Version: 4.7.0
Homepage: https://mypaint.app
Vcs-Git: https://salsa.debian.org/python-team/packages/mypaint.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/mypaint
Rules-Requires-Root: no

Package: mypaint
Architecture: any
Depends:
 gir1.2-gtk-3.0,
 mypaint-data (>= ${source:Version}),
 mypaint-brushes (>= 1.5~),
 python3-gi,
 python3-gi-cairo,
 python3-numpy,
 librsvg2-bin,
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends}
Recommends:
 mypaint-data-extras,
 shared-mime-info,
Description: paint program for use with graphics tablets
 MyPaint is a pressure- and tilt-sensitive painting program which works well
 with Wacom graphics tablets and other similar devices. It comes with a large
 brush collection including charcoal and ink to emulate real media, but the
 highly configurable brush engine allows you to experiment with your own
 brushes and with not-quite-natural painting.
 .
 This package contains the main program.

Package: mypaint-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: runtime data files for MyPaint
 MyPaint is a pressure- and tilt-sensitive painting program which works well
 with Wacom graphics tablets and other similar devices. It comes with a large
 brush collection including charcoal and ink to emulate real media, but the
 highly configurable brush engine allows you to experiment with your own
 brushes and with not-quite-natural painting.
 .
 This package contains icons and backgrounds for the MyPaint program.

Package: mypaint-data-extras
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Replaces: mypaint-data-hires
Conflicts: mypaint-data-hires
Enhances: mypaint
Description: high resolution backgrounds for mypaint
 MyPaint is a pressure- and tilt-sensitive painting program which works well
 with Wacom graphics tablets and other similar devices. It comes with a large
 brush collection including charcoal and ink to emulate real media, but the
 highly configurable brush engine allows you to experiment with your own
 brushes and with not-quite-natural painting.
 .
 This package contains high resolution paper texture backgrounds for painting
 on. Since the files are large, installation is optional.
